class GUI.Base
{
#protected:
	_visible = false
	_isDisabled = false

#public:
	parent = null
	metadata = null

	constructor(arg)
	{
		parent = "parent" in arg ? arg.parent : parent
		metadata = "metadata" in arg ? arg.metadata : {}

		if ("visible" in arg)
			setVisible(arg.visible)

		if ("disabled" in arg)
			setDisabled(arg.disabled)

		if ("collection" in arg)
			arg.collection.insert(this)
	}

	function getVisible()
	{
		return _visible
	}

	function setVisible(visible)
	{
		_visible = visible
	}

	function getDisabled()
	{
		return _isDisabled
	}

	function setDisabled(disabled)
	{
		_isDisabled = disabled
	}

	function getPosition()
	{
		local positionPx = getPositionPx()
		return { x = anx(positionPx.x), y = any(positionPx.y) }
	}

	function setPosition(x, y)
	{
		setPositionPx(nax(x), nay(y))
	}

	function getSize()
	{
		local sizePx = getSizePx()
		return { width = anx(sizePx.width), height = any(sizePx.height) }
	}
	
	function setSize(width, height)
	{
		setSizePx(nax(width), nay(height))
	}
}