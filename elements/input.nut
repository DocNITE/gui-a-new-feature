enum Input
{
	Text,
	Password,
	Numbers
}

local ref =
{
	activeInput = null
}

local GUIInputClasses = classes(GUI.Texture, GUI.Alignment)
class GUI.Input extends GUIInputClasses
{
#private:
	_type = null

	_inputColor = null
	_inputAlpha = 255

	_placeholderColor = null
	_placeholderAlpha = 255
	_placeholderText = ""

	_paddingPx = 2
	_text = ""
	_hashSymbol = "#"

	_isDotPresent = false
	_isScientificEPresent = false
#public:
	draw = null
	selector = "|"
	maxLetters = 1000

	constructor(arg)
	{
		_type = "type" in arg ? arg.type : Input.Text

		_inputColor = "inputColor" in arg ? arg.inputColor : {r = 255, g = 255, b = 255}
		_inputAlpha = "inputAlpha" in arg ? arg.inputAlpha : _inputAlpha

		_placeholderColor = "placeholderColor" in arg ? arg.placeholderColor : {r = 255, g = 255, b = 255}
		_placeholderAlpha = "placeholderAlpha" in arg ? arg.placeholderAlpha : _placeholderAlpha
		_placeholderText = "placeholderText" in arg ? arg.placeholderText : _placeholderText
		
		if ("paddingPx" in arg)
			_paddingPx = arg.paddingPx
		else if ("margin" in arg)
			_paddingPx = nax(arg.margin)

		_text = "text" in arg ? arg.text : _text
		_hashSymbol = "hashSymbol" in arg ? arg.hashSymbol : _hashSymbol
		selector = "selector" in arg ? arg.selector : selector
		maxLetters = "maxLetters" in arg ? arg.maxLetters : maxLetters

		draw = GUI.Draw("draw" in arg ? arg.draw : null)
		draw.setDisabled(true)
		draw.setColor(_placeholderColor.r, _placeholderColor.g, _placeholderColor.b)
		draw.setAlpha(_placeholderAlpha)

		GUI.Texture.constructor.call(this, arg)
		draw.setText(_text)
		
		setAlignment("align" in arg ? arg.align : Align.Left)
		bind(EventType.MouseDown, onMouseDown.bindenv(this))
	}

	function setVisible(bool)
	{
		GUI.Texture.setVisible.call(this, bool)
		draw.setVisible(bool)
	}

	function setDisabled(disabled)
	{
		GUI.Texture.setDisabled.call(this, disabled)
		if(disabled && getActive())
			setActive(false)
	}

	function setPositionPx(x, y)
	{
		GUI.Texture.setPositionPx.call(this, x, y)
		updateDrawPosition()
	}

	function setSizePx(width, height)
	{
		GUI.Texture.setSizePx.call(this, width, height)
		updateDrawPosition()
	}

	function setAlignment(alignment)
	{
		GUI.Alignment.setAlignment.call(this, alignment)
		updateDrawPosition()
	}

	function getType()
	{
		return _type
	}

	function setType(type)
	{
		if (type == Input.Numbers && _type != Input.Numbers)
		{
			_type = type
			setText("")
		}
		else
		{
			_type = type
			setText(getText())
		}
	}

	function getText()
	{
		return _text
	}

	function setText(text)
	{
		_text = text

		local active = getActive()
		if(!active && _text == "")
		{
			draw.setText(_placeholderText)
			draw.setColor(_placeholderColor.r, _placeholderColor.g, _placeholderColor.b)
			draw.setAlpha(_placeholderAlpha)

			updateDrawPosition()
			return
		}

		if(_type == Input.Numbers)
		{
			_isDotPresent = text.find(".") != null
			_isScientificEPresent = text.find("e") != null || text.find("E") != null
		}

		local curText = (_type == Input.Password) ? cutText(hash(_text)) : cutText(_text)
		draw.setText(active ? curText + selector : curText)
		updateDrawPosition()
	}

	function getInputColor()
	{
		return _inputColor
	}

	function setInputColor(r, g, b)
	{
		_inputColor.r = r
		_inputColor.g = g
		_inputColor.b = b

		if(getActive())
			draw.setColor(r, g, b)
	}

	function getInputAlpha()
	{
		return _inputAlpha
	}

	function setInputAlpha(alpha)
	{
		_inputAlpha = alpha

		if(getActive())
			draw.setAlpha(alpha)
	}

	function getPlaceholderColor()
	{
		return _placeholderColor
	}

	function setPlaceholderColor(r, g, b)
	{
		_placeholderColor.r = r
		_placeholderColor.g = g
		_placeholderColor.b = b

		if(!getActive() && _text == "")
			draw.setColor(r, g, b)
	}

	function setPlaceholderAlpha(alpha)
	{
		_placeholderAlpha = alpha

		if(!getActive() && _text == "")
			draw.setAlpha(alpha)
	}

	function getPlaceholderAlpha()
	{
		return _placeholderAlpha
	}

	function getPaddingPx()
	{
		return _paddingPx
	}

	function setPaddingPx(padding)
	{
		_paddingPx = padding
		updateDrawPosition()
	}

	function getPadding()
	{
		return anx(_paddingPx)
	}

	function setPadding(padding)
	{
		setPaddingPx(nax(padding))
	}

	function getPlaceholderText()
	{
		return _placeholderText
	}

	function setPlaceholderText(text)
	{
		if(!getActive())
		{
			draw.setText(text)
			updateDrawPosition()
		}

		_placeholderText = text
	}

	function getHashSymbol()
	{
		return _hashSymbol
	}

	function setHashSymbol(hashSymbol)
	{
		_hashSymbol = hashSymbol
		setText(_text)
	}

	function getActive()
	{
		return ref.activeInput == this
	}

	function setActive(active)
	{
		if (active && ref.activeInput && ref.activeInput != this)
			ref.activeInput.setActive(false)

		ref.activeInput = (active) ? this.weakref() : null

		setText(_text)
	}

	function cutText(text)
	{
		local sizePx = getSizePx()
		local finishText = ""

		local oldFont = textGetFont()
		textSetFont(draw.getFont())

		for (local i = text.len(); i > 0; i--)
		{
			local char = text.slice(i-1, i);
			if(textWidthPx(finishText + char) < sizePx.width - (2 * _paddingPx))
				finishText = char + finishText
			else
			{
				textSetFont(oldFont)
				return finishText
			}
		}

		textSetFont(oldFont)
		return finishText
	}

	function hash(text)
	{
		local endText = ""
		for (local i = 0, len = text.len(); i < len; i++)
			endText += _hashSymbol

		return endText
	}

	function removeLetter()
	{
		if (_text.len() < 1)
			return

		if (getDisabled())
			return

		setText(_text.slice(0, _text.len()-1))
		call(EventType.RemoveLetter, _text)
		callEvent("GUI.onInputRemoveLetter", this, _text)
	}

	function addLetter(key)
	{
		if(_text.len() >= maxLetters)
			return

		if (getDisabled())
			return

		local letter = getKeyLetter(key)
		if(!letter)
			return

		if(_type == Input.Numbers)
		{
			if((_text.len() == 0 && (letter == "-" || letter == "+"))
			|| (!_isDotPresent && (letter == "."))
			|| (!_isScientificEPresent && (letter == "e" || letter == "E"))
			|| letter == "0" || letter == "1" || letter == "2" || letter == "3" || letter == "4"
			|| letter == "5" || letter == "6" || letter == "7" || letter == "8" || letter == "9" || letter == "0")
				_text += letter
		}
		else
			_text += letter

		setText(_text)
		call(EventType.InsertLetter, letter)
		callEvent("GUI.onInputInsertLetter", this, letter)
	}

	function updateDrawPosition()
	{
		local positionPx = getPositionPx()
		local sizePx = getSizePx()
		local sizeDrawPx = draw.getSizePx()
		
		switch(_alignment)
		{
			case Align.Left:
				draw.setPositionPx(positionPx.x + _paddingPx, positionPx.y + sizePx.height / 2 - sizeDrawPx.height/2)
				break
				
			case Align.Center:
				draw.setPositionPx(positionPx.x + sizePx.width/2 - sizeDrawPx.width / 2, positionPx.y + sizePx.height/2 - sizeDrawPx.height/2)
				break

			case Align.Right:
				draw.setPositionPx(positionPx.x + sizePx.width - (sizeDrawPx.width + _paddingPx), positionPx.y + sizePx.height/2 - sizeDrawPx.height/2)
				break
		}
	}

	function onMouseDown(self, button)
	{
		if (button != MOUSE_LMB)
			return

		if (ref.activeInput == this)
			return

		setActive(true)
	}

	static function onKey(key)
	{
		if (!ref.activeInput)
			return

		cancelEvent()

		if(key == KEY_BACK)
			ref.activeInput.removeLetter()
		else
			ref.activeInput.addLetter(key)
	}

	static function onMouseClick(button)
	{
		if (button != MOUSE_LMB)
			return

		if (!ref.activeInput)
			return

		if (GUI.Event.getElementPointedByCursor() instanceof this)
			return

		ref.activeInput.setActive(false)
	}

	static function hasActiveInput()
	{
		return ref.activeInput != null
	}
}

addEventHandler("onKey", GUI.Input.onKey)
addEventHandler("onMouseClick", GUI.Input.onMouseClick.bindenv(GUI.Input))