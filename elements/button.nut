local GUIButtonClasses = classes(GUI.Texture, GUI.Alignment, GUI.Offset)
class GUI.Button extends GUIButtonClasses
{
#public:
	draw = null

	constructor(arg)
	{
		if ("draw" in arg)
		{
			draw = GUI.Draw(arg.draw)
			draw.setDisabled(true)

			GUI.Offset.constructor.call(this, arg)
			_alignment = "align" in arg ? arg.align : Align.Center
		}

		GUI.Texture.constructor.call(this, arg)
		if (draw)
			updateDrawPosition()
	}

	function setOffsetPx(x, y)
	{
		GUI.Offset.setOffsetPx.call(this, x, y)

		if (draw)
			updateDrawPosition()
	}

	function setAlignment(alignment)
	{
		GUI.Alignment.setAlignment.call(this, alignment)

		if (draw)
			updateDrawPosition()
	}

	function setVisible(visible)
	{
		GUI.Texture.setVisible.call(this, visible)

		if (draw)
			draw.setVisible(visible)
	}

	function setAlpha(alpha)
	{
		GUI.Texture.setAlpha.call(this, alpha)

		if (draw)
			draw.setAlpha(alpha)
	}

	function top()
	{
		GUI.Texture.top.call(this)

		if (draw)
			draw.top()
	}

	function setPositionPx(x, y)
	{
		GUI.Texture.setPositionPx.call(this, x, y)

		if (draw)
			updateDrawPosition()
	}

	function setSizePx(width, height)
	{
		GUI.Texture.setSizePx.call(this, width, height)

		if (draw)
			updateDrawPosition()
	}

	function getText()
	{
		if (!draw)
			return ""

		return draw.getText()
	}

	function setText(text)
	{
		if (!draw)
			return

		draw.setText(text)
		updateDrawPosition()
	}

	function getFont()
	{
		if (!draw)
			return ""

		return draw.getFont()
	}

	function setFont(font)
	{
		if (!draw)
			return

		draw.setFont(font)
		updateDrawPosition()
	}

	function updateDrawPosition()
	{
		local positionPx = getPositionPx()
		local offsetPx = getOffsetPx()
		local sizePx = getSizePx()
		local drawSizePx = draw.getSizePx()
		local drawPositionXPx = positionPx.x + offsetPx.x
		local drawPositionYPx = positionPx.y + offsetPx.y + (sizePx.height - drawSizePx.height) / 2

		switch (_alignment)
		{
			case Align.Left:
				draw.setPositionPx(drawPositionXPx, drawPositionYPx)
				break

			case Align.Center:
				draw.setPositionPx(drawPositionXPx + (sizePx.width - drawSizePx.width) / 2, drawPositionYPx)
				break

			case Align.Right:
				draw.setPositionPx(drawPositionXPx + sizePx.width - drawSizePx.width, drawPositionYPx)
				break
		}		
	}
}