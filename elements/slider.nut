class GUI.Slider extends GUI.Range
{
#public:
	bar = null

	constructor(arg)
	{
		bar = GUI.Bar("bar" in arg ? arg.bar : null)
		base.constructor("range" in arg ? arg.range : null)

		bar.top()
		indicator.top()

		setMinimum(_minimum)
		setMaximum(_maximum)
		setValue(_value)

		if (getAlignment() == Align.Right)
			swapMinMax()

		local sizePx = getSizePx()
		local positionPx = getPositionPx()
		local marginPx = getMarginPx()

		bar.setSizePx(sizePx.width, sizePx.height)
		bar.setPositionPx(positionPx.x, positionPx.y)
		bar.setMarginPx(marginPx.top, marginPx.right, marginPx.bottom, marginPx.left)
	}

	function swapMinMax()
	{
		local minimum = getMinimum()
		local maximum = getMaximum()

		local value = getValue()

		base.setMaximum(minimum)
		base.setMinimum(maximum)

		setValue(value)
	}

	function setMinimum(minimum)
	{
		base.setMinimum(minimum)
		bar.setMinimum(minimum)
	}

	function setMaximum(maximum)
	{
		base.setMaximum(maximum)
		bar.setMaximum(maximum)
	}

	function setValue(value)
	{
		base.setValue(value)
		bar.setValue(value)
	}

	function getAlignment()
	{
		return bar.getAlignment()
	}

	function setAlignment(alignment)
	{
		if (alignment == getAlignment())
			return

		swapMinMax()
		bar.setAlignment(alignment)
	}

	function setVisible(visible)
	{
		base.setVisible(visible)
		bar.setVisible(visible)
	}

	function setDisabled(disabled)
	{
		base.setDisabled(disabled)
		bar.setDisabled(disabled)
	}

	function top()
	{
		base.top()

		bar.top()
		indicator.top()
	}

	function setAlpha(alpha)
	{
		base.setAlpha(alpha)
		bar.setAlpha(alpha)
	}

	function setPositionPx(x, y)
	{
		base.setPositionPx(x, y)
		bar.setPositionPx(x, y)
	}

	function setSizePx(width, height)
	{
		base.setSizePx(width, height)
		bar.setSizePx(width, height)
	}

	function setMarginPx(top, right, bottom, left)
	{
		base.setMarginPx(top, right, bottom, left)
		bar.setMarginPx(top, right, bottom, left)
	}
}