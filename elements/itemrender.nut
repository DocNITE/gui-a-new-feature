local GUIItemRenderClasses = classes(ItemRender, GUI.Event)
class GUI.ItemRender extends GUIItemRenderClasses {
	constructor(x, y, width, height, instance, window = null)
	{
		GUI.Event.constructor.call(this)
		ItemRender.constructor.call(this, x, y, width, height, instance)

		if (window)
			window.insert(this)
	}

	function setPositionPx(x, y)
	{
		ItemRender.setPositionPx.call(this, x, y)
		GUI.Event.setPositionPx.call(this, x, y)
	}

	function setPosition(x, y)
	{
		ItemRender.setPosition.call(this, x, y)
		GUI.Event.setPosition.call(this, x, y)
	}

	function setSizePx(width, height)
	{
		ItemRender.setSizePx.call(this, width, height)
		GUI.Event.setSizePx.call(this, width, height)
	}

	function setSize(width, height)
	{
		ItemRender.setSize.call(this, width, height)
		GUI.Event.setSize.call(this, width, height)
	}

	function top()
	{
		GUI.Event.top.call(this)
		ItemRender.top.call(this)
	}

	function getVisible()
	{
		return	this.visible
	}

	function setVisible(visible)
	{
		GUI.Event.setVisible.call(this, visible)
		this.visible = visible
	}

	function setRotation(rotX, rotY, rotZ)
	{
		this.rotX = rotX
		this.rotY = rotY
		this.rotZ = rotZ
	}

	function getRotation(rotX, rotY, rotZ)
	{
		return	{
			rotX = this.rotX,
			rotY = this.rotY,
			rotZ = this.rotZ,
		}
	}

	function setZbias(zbias)
	{
		this.zbias = zbias
	}

	function getZbias()	{
		return this.zbias
	}

	function setLightingswell(lightingswell)
	{
		this.lightingswell = lightingswell
	}

	function getLightingswell()
	{
		return this.lightingswell
	}

	function setVisual(visual)
	{
		this.visual = visual
	}

	function getVisual()
	{
		return this.visual
	}

	function setInstance(instance)
	{
		this.instance = instance
	}

	function getInstance()
	{
		return this.instance
	}
}
