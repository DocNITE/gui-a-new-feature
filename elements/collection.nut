class GUI.Collection extends GUI.Base
{
#private:
	_positionPx = null

#public:
	childs = null

	constructor(arg)
	{
		if ("positionPx" in arg)
			_positionPx = {x = arg.positionPx.x, y = arg.positionPx.y}
		else if("position" in arg)
			_positionPx = {x = nax(arg.position.x), y = nay(arg.position.y)}
		else
			_positionPx = {x = 0, y = 0}

		childs = []
		if ("childs" in arg)
		{
			foreach (child in arg.childs)
				insert(child)
		}

		base.constructor(arg)
	}

	function insert(pointer)
	{
		if (getChildIdx(pointer) != -1)
			return

		local positionPx = getPositionPx()
		local childPositionPx = pointer.getPositionPx()

		childs.push({offset = clone childPositionPx, pointer = pointer})
		pointer.setPositionPx(positionPx.x + childPositionPx.x, positionPx.y + childPositionPx.y)
	}

	function remove(pointer)
	{
		childs.remove(getChildIdx(pointer))
	}

	function getChildIdx(pointer)
	{
		for(local i = 0, end = childs.len(); i < end; ++i)
		{
			if (childs[i].pointer == pointer)
				return i
		}

		return -1
	}

	function getPositionPx()
	{
		return _positionPx
	}

	function setPositionPx(x, y)
	{
		_positionPx.x = x
		_positionPx.y = y

		foreach(item in childs)
			item.pointer.setPositionPx(item.offset.x + x, item.offset.y + y)
	}

	function getChildPositionPx(pointer)
	{
		return childs[getChildIdx(pointer)]
	}

	function setChildPositionPx(pointer, x, y)
	{
		local positionPx = getPositionPx()
		local child = childs[getChildIdx(pointer)]

		child.offset.x = x
		child.offset.y = y

		pointer.setPositionPx(positionPx.x + x, positionPx.y + y)
	}

	function getChildPosition(pointer)
	{
		local position = getChildPositionPx(pointer)
		return {x = anx(position.x), y = anx(position.y)}
	}

	function setChildPosition(pointer, x, y)
	{
		setChildPositionPx(pointer, nax(x), nay(y))
	}

	function setVisible(visible)
	{
		foreach (item in childs)
			item.pointer.setVisible(visible)
		
		base.setVisible(visible)
	}

	function setDisabled(disabled)
	{
		foreach (item in childs)
			item.pointer.setDisabled(disabled)
		
		base.setDisabled(disabled)
	}

	function top()
	{
		foreach (item in childs)
			item.pointer.top()
	}
}
