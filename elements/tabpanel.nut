local ref =
{
	movableTab = null
}

local cursorOffset = 0

class GUI.Tab extends GUI.Button
{

#public:
	id = -1

	constructor(arg)
	{
		id = "id" in arg ? arg.id : id
		base.constructor(arg)

		this.bind(EventType.MouseDown, onMouseDown)
	}

	function onMouseDown(self, btn)
	{
		if (btn != MOUSE_LMB)
			return

		local panel = self.parent
		if (!panel._tabsMovementEnabled)
			return

		local positionPx = self.getPositionPx()
		local cursorPositionPx = getCursorPositionPx()

		switch (panel.getOrientation())
		{
			case Orientation.Horizontal:
				cursorOffset = cursorPositionPx.x - positionPx.x
				break

			case Orientation.Vertical:
				cursorOffset = cursorPositionPx.y - positionPx.y
				break
		}

		self.top()
		panel.setActiveTab(self.id)
		ref.movableTab = self.weakref()
	}

	static function onMouseRelease(button)
	{
		if (button != MOUSE_LMB)
			return

		if (!ref.movableTab)
			return

		local panel = ref.movableTab.parent
		local orientation = panel.getOrientation()

		local positionPx = panel.getPositionPx()
		local marginPx = panel.getMarginPx()

		positionPx.x += marginPx.left
		positionPx.y += marginPx.top

		for (local i = 0; i < ref.movableTab.id; ++i)
		{
			local sizePx = panel.tabs[i].getSizePx()

			switch (orientation)
			{
				case Orientation.Horizontal:
					positionPx.x += sizePx.width
					break

				case Orientation.Vertical:
					positionPx.y += sizePx.height
					break
			}
		}

		ref.movableTab.setPositionPx(positionPx.x, positionPx.y)
		ref.movableTab = null
	}

	static function onMouseMove(x, y)
	{
		if (!isMouseBtnPressed(MOUSE_LMB))
			return

		if (!ref.movableTab)
			return

		local cursorPositionPx = getCursorPositionPx()
		local cursorSensitivity = getCursorSensitivity()

		local newCursorX = cursorPositionPx.x
		local newCursorY = cursorPositionPx.y
		local oldCursorX = cursorPositionPx.x - x * cursorSensitivity
		local oldCursorY = cursorPositionPx.y - y * cursorSensitivity

		local tabPosition = ref.movableTab.getPositionPx()
		local tabSize = ref.movableTab.getSizePx()

		local panel = ref.movableTab.parent
		local tabsCount = panel.tabs.len()

		local dimension, size
		local minimumPanelRange, maximumPanelRange
		local cursorMovingDirection

		// Moving tab, keeping tab in panel range,
		//determinating the cursor movement direction

		if (panel._orientation == Orientation.Horizontal)
		{
			dimension = "x"
			size = "width"

			tabPosition.x = newCursorX - cursorOffset
			minimumPanelRange = maximumPanelRange = panel.getPositionPx().x

			cursorMovingDirection = (newCursorX - oldCursorX) < 0 ? -1 : 1
		}
		else if (panel._orientation == Orientation.Vertical)
		{
			dimension = "y"
			size = "height"

			tabPosition.y = newCursorY -cursorOffset
			minimumPanelRange = maximumPanelRange = panel.getPositionPx().y

			cursorMovingDirection = (newCursorY - oldCursorY) < 0 ? -1 : 1
		}

		for (local i = 0; i < tabsCount - 1; ++i)
			maximumPanelRange += panel.tabs[i].getSizePx()[size]

		if (tabPosition[dimension] < minimumPanelRange)
			tabPosition[dimension] = minimumPanelRange
		else if (tabPosition[dimension] > maximumPanelRange)
			tabPosition[dimension] = maximumPanelRange

		// Checking if tab can be swapped,
		//swapping tab with nearest tab, if our tab is on half way to the nearest tab

		if ((cursorMovingDirection == -1 && ref.movableTab.id > 0)
		|| (cursorMovingDirection == 1 && ref.movableTab.id < tabsCount - 1))
		{
			local nearTab = panel.tabs[ref.movableTab.id + cursorMovingDirection]
			local nearTabPosition = nearTab.getPositionPx()
			local nearTabSize = nearTab.getSizePx()

			if ((cursorMovingDirection == -1 && tabPosition[dimension] < nearTabPosition[dimension] + nearTabSize[size] / 2)
			|| (cursorMovingDirection == 1 && tabPosition[dimension] >= nearTabPosition[dimension] - nearTabSize[size] / 2))
			{
				if (panel._orientation == Orientation.Horizontal)
					nearTab.setPositionPx(nearTabPosition.x + tabSize.width * -cursorMovingDirection, nearTabPosition.y)
				else
					nearTab.setPositionPx(nearTabPosition.x, nearTabPosition.y + tabSize.height * -cursorMovingDirection)

				panel.tabs[ref.movableTab.id + cursorMovingDirection] = ref.movableTab
				panel.tabs[ref.movableTab.id] = nearTab

				nearTab.id -= cursorMovingDirection
				ref.movableTab.id += cursorMovingDirection
			}
		}

		ref.movableTab.setPositionPx(tabPosition.x, tabPosition.y)
	}
}

addEventHandler("onMouseRelease", GUI.Tab.onMouseRelease)
addEventHandler("onMouseMove", GUI.Tab.onMouseMove)

local GUITabPanelClasses = classes(GUI.Texture, GUI.Orientation, GUI.Margin)
class GUI.TabPanel extends GUITabPanelClasses
{
#private:
	_tabsMovementEnabled = true

	_font = "FONT_OLD_10_WHITE_HI.TGA"

	_activeTab = null
	_tabSizePx = null

#public:
	tabs = null

	constructor(arg)
	{
		tabs = []

		_tabSizePx = {width = 0, height = 0}
		GUI.Margin.constructor.call(this, arg)
		_orientation = "orientation" in arg ? arg.orientation : Orientation.Horizontal
		GUI.Texture.constructor.call(this, arg)
		updateSize()

		if ("tabs" in arg)
		{
			foreach (tab in arg.tabs)
				addTab(tab)
		}
	}

	function insertTab(arg)
	{
		local tab = GUI.Tab(arg)
		tab.parent = this

		if (tab.draw)
			tab.draw.setFont(_font)

		tabs.insert(tab.id, tab)

		local tabsCount = tabs.len()
		for (local i = tab.id + 1; i < tabsCount; ++i)
			++tabs[i].id

		updateSize()

		if (visible)
			tab.setVisible(true)

		return tab
	}

	function addTab(arg)
	{
		arg["id"] <- tabs.len()
		return insertTab(arg)
	}

	function removeTab(id)
	{
		local tabsCount = tabs.len()

		if (!tabsCount)
			return

		for (local i = id; i < tabsCount; ++i)
			--tabs[i].id

		if (_activeTab == tabs[id])
		{
			if (tabsCount == 1)
				setActiveTab(null)
			else if (id + 1 < tabsCount)
				setActiveTab(id + 1)
			else
				setActiveTab(id - 1)
		}

		tabs.remove(id)
		updateSize()
	}

	function setMarginPx(top, right, bottom, left)
	{
		GUI.Margin.setMarginPx.call(this, top, right, bottom, left)
		updateSize()
	}

	function getActiveTab()
	{
		return _activeTab
	}

	function setActiveTab(id)
	{
		local tab = (id != null) ? tabs[id] : null

		call(EventType.Switch, tab, _activeTab)
		callEvent("GUI.onSwitch", tab, _activeTab)

		_activeTab = tab
	}

	function getTabsMovement()
	{
		return _tabsMovementEnabled
	}

	function setTabsMovement(movement)
	{
		_tabsMovementEnabled = movement
	}

	function setPositionPx(x, y)
	{
		GUI.Texture.setPositionPx.call(this, x, y)
		updatePosition()
	}

	function setSizePx(width, height)
	{
		GUI.Texture.setSizePx.call(this, width, height)
		updateSize()
	}

	function setVisible(visible)
	{
		GUI.Texture.setVisible.call(this, visible)

		foreach (tab in tabs)
			tab.setVisible(visible)

		if (visible)
		{
			if (!_activeTab && (0 in tabs))
				setActiveTab(0)
		}
		else
			setActiveTab(null)
	}

	function top()
	{
		GUI.Texture.top.call(this)

		foreach (tab in tabs)
			tab.top()
	}

	function getFont()
	{
		return _font
	}

	function setFont(font)
	{
		_font = font

		foreach (tab in tabs)
			tab.setFont(font)
	}

	function updatePosition()
	{
		local positionPx = getPositionPx()
		local marginPx = getMarginPx()

		local x = positionPx.x + marginPx.left
		local y = positionPx.y + marginPx.top

		foreach (tab in tabs)
		{
			tab.setPositionPx(x, y)
			switch (_orientation)
			{
				case Orientation.Horizontal:
					x += _tabSizePx.width
					break

				case Orientation.Vertical:
					y += _tabSizePx.height
					break
			}	
		}
	}

	function updateSize()
	{
		local tabsCount = tabs.len()
		if (!tabsCount)
			return

		local sizePx = getSizePx()
		local marginPx = getMarginPx()

		sizePx.width -= marginPx.left + marginPx.right
		sizePx.height -= marginPx.top + marginPx.bottom

		switch (_orientation)
		{
			case Orientation.Horizontal:
				_tabSizePx.width = sizePx.width / tabsCount
				_tabSizePx.height = sizePx.height
				break

			case Orientation.Vertical:
				_tabSizePx.width = sizePx.width
				_tabSizePx.height = sizePx.height / tabsCount
				break
		}

		foreach (tab in tabs)
			tab.setSizePx(_tabSizePx.width, _tabSizePx.height)

		updatePosition()
	}
}
